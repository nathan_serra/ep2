/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transportadora;

/**
 *
 * @author nathan
 */
public class MotoAlcool extends Veiculo {
    private float RendimentoPerdido;
    
    public MotoAlcool(){
        setNome("moto");
        setCombustivel("alcool");
        setRendimento(50.0f);
        setVelocidadeMedia(110.0f);
        setCargaMaxima(50.0f);
        setRendimentoPerdido(0.3f);
    }
    
    public float calcularCusto(float DistanciaViagem, float QtdCarga){
        float Rendimento = getRendimento();
        float PrecoCombustivel;
        float QtdCombustivel;
        float RendimentoPerdido = getRendimentoPerdido();
        PrecoCombustivel = 3.499f;
        Rendimento = Rendimento - RendimentoPerdido * QtdCarga;
        QtdCombustivel = DistanciaViagem/Rendimento;
       return QtdCombustivel * PrecoCombustivel;
    }

    public float getRendimentoPerdido() {
        return RendimentoPerdido;
    }

    public void setRendimentoPerdido(float RendimentoPerdido) {
        this.RendimentoPerdido = RendimentoPerdido;
    }
    
    
}
