/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transportadora;

/**
 *
 * @author nathan
 */
public class Carreta extends Veiculo {
    private float RendimentoPerdido;
    
    public Carreta(){
        setNome("carreta");
        setCombustivel("diesel");
        setRendimento(8.0f);
        setVelocidadeMedia(60.0f);
        setCargaMaxima(30000.0f);
        setRendimentoPerdido(0.0002f);
    }
    
    public float calcularCusto(float DistanciaViagem, float QtdCarga){
        float Rendimento = getRendimento();
        float PrecoCombustivel;
        float QtdCombustivel;
        float RendimentoPerdido = getRendimentoPerdido();
        PrecoCombustivel = 3.869f;
        Rendimento = Rendimento - RendimentoPerdido * QtdCarga;
        QtdCombustivel = DistanciaViagem/Rendimento;
       return QtdCombustivel * PrecoCombustivel;
    }

    public float getRendimentoPerdido() {
        return RendimentoPerdido;
    }

    public void setRendimentoPerdido(float RendimentoPerdido) {
        this.RendimentoPerdido = RendimentoPerdido;
    }
    
    
}
