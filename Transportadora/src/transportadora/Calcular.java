/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transportadora;
    
/**
 *
 * @author nathan
 */
public class Calcular extends javax.swing.JFrame {
    /**
     * Creates new form Calcular
     */
    private float pesoFloat;
    private float distanciaFloat;
    private float lucroFloat;
    private float tempoMaximoFloat;
    String Escolha;

    public float getPesoFloat() {
        return pesoFloat;
    }

    public void setPesoFloat(float pesoFloat) {
        this.pesoFloat = pesoFloat;
    }

    public float getDistanciaFloat() {
        return distanciaFloat;
    }

    public void setDistanciaFloat(float distanciaFloat) {
        this.distanciaFloat = distanciaFloat;
    }

    public float getLucroFloat() {
        return lucroFloat;
    }

    public void setLucroFloat(float lucroFloat) {
        this.lucroFloat = lucroFloat;
    }

    public float getTempoMaximoFloat() {
        return tempoMaximoFloat;
    }

    public void setTempoMaximoFloat(float tempoMaximoFloat) {
        this.tempoMaximoFloat = tempoMaximoFloat;
    }
    
    
    public Calcular() {
        initComponents();
        setLocationRelativeTo(null);
        finalizar.setEnabled(false);
        opcaoMaisRapida.setEnabled(false);
        OpcaoMenorCusto.setEnabled(false);
        opcaoMenorCustoBeneficio.setEnabled(false);
        lucro.setEnabled(false);
        custo.setEnabled(false);
    }
    
    public void EfetuarCalculo(String veiculo){
        Carro c = new Carro();
        CarroAlcool ca = new CarroAlcool();
        MotoGasolina mg = new MotoGasolina();
        MotoAlcool ma = new MotoAlcool();
        Van v = new Van();
        Carreta cr = new Carreta();
        
        float aux;
        
        switch(veiculo){
            case "carro gasolina":
                custo.setText(String.valueOf(c.calcularCusto(distanciaFloat, pesoFloat)));
                aux = c.calcularCusto(distanciaFloat, pesoFloat);
                this.lucroFloat = this.lucroFloat/100;
                this.lucroFloat = (aux/(1-this.lucroFloat)) - aux;
                lucro.setText(String.valueOf(this.lucroFloat));
                break;
            case "carro alcool":
                custo.setText(String.valueOf(ca.calcularCusto(distanciaFloat, pesoFloat)));
                aux = ca.calcularCusto(distanciaFloat, pesoFloat);
                this.lucroFloat = this.lucroFloat/100;
                this.lucroFloat = (aux/(1-this.lucroFloat));
                lucro.setText(String.valueOf(this.lucroFloat));
                break;
            case "moto gasolina":
                custo.setText(String.valueOf(mg.calcularCusto(distanciaFloat, pesoFloat)));
                aux = mg.calcularCusto(distanciaFloat, pesoFloat);
                this.lucroFloat = this.lucroFloat/100;
                this.lucroFloat = (aux/(1-this.lucroFloat));
                lucro.setText(String.valueOf(this.lucroFloat));
                break;
            case "moto alcool":
                custo.setText(String.valueOf(ma.calcularCusto(distanciaFloat, pesoFloat)));
                aux = ma.calcularCusto(distanciaFloat, pesoFloat);
                this.lucroFloat = this.lucroFloat/100;
                this.lucroFloat = (aux/(1-this.lucroFloat));
                lucro.setText(String.valueOf(this.lucroFloat));
                break;
            case "van":
                custo.setText(String.valueOf(v.calcularCusto(distanciaFloat, pesoFloat)));
                aux = v.calcularCusto(distanciaFloat, pesoFloat);
                this.lucroFloat = this.lucroFloat/100;
                this.lucroFloat = (aux/(1-this.lucroFloat));
                lucro.setText(String.valueOf(this.lucroFloat));
                break;
            case "carreta":
                custo.setText(String.valueOf(cr.calcularCusto(distanciaFloat, pesoFloat)));
                aux = cr.calcularCusto(distanciaFloat, pesoFloat);
                this.lucroFloat = this.lucroFloat/100;
                this.lucroFloat = (aux/(1-this.lucroFloat));
                lucro.setText(String.valueOf(this.lucroFloat));
                break;
            default: System.out.println("erro");
        }
        
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        opcaoMaisRapida = new javax.swing.JTextField();
        OpcaoMenorCusto = new javax.swing.JTextField();
        opcaoMenorCustoBeneficio = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        opcaoRapida = new javax.swing.JRadioButton();
        opcaoCusto = new javax.swing.JRadioButton();
        opcaoCustoBeneficio = new javax.swing.JRadioButton();
        finalizar = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        custo = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        lucro = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Calculo dos veículos!");

        opcaoMaisRapida.setText(" ");
        opcaoMaisRapida.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opcaoMaisRapidaActionPerformed(evt);
            }
        });

        OpcaoMenorCusto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OpcaoMenorCustoActionPerformed(evt);
            }
        });

        opcaoMenorCustoBeneficio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opcaoMenorCustoBeneficioActionPerformed(evt);
            }
        });

        jLabel1.setText("Opções frete");

        buttonGroup1.add(opcaoRapida);
        opcaoRapida.setText("Opção mais rápida:");
        opcaoRapida.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opcaoRapidaActionPerformed(evt);
            }
        });

        buttonGroup1.add(opcaoCusto);
        opcaoCusto.setText("Opção com menor custo:");
        opcaoCusto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opcaoCustoActionPerformed(evt);
            }
        });

        buttonGroup1.add(opcaoCustoBeneficio);
        opcaoCustoBeneficio.setText("Opção melhor custo benefício:");
        opcaoCustoBeneficio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opcaoCustoBeneficioActionPerformed(evt);
            }
        });

        finalizar.setText("Finalizar");
        finalizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                finalizarActionPerformed(evt);
            }
        });

        jLabel2.setText("custo:");

        custo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                custoActionPerformed(evt);
            }
        });

        jLabel3.setText("lucro:");

        lucro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lucroActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(128, 128, 128)
                                .addComponent(jLabel1))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(opcaoCusto)
                                    .addComponent(opcaoRapida)
                                    .addComponent(opcaoCustoBeneficio))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(opcaoMaisRapida, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(OpcaoMenorCusto, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(opcaoMenorCustoBeneficio, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel3))
                                .addGap(202, 202, 202)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(custo)
                                    .addComponent(lucro)))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(155, 155, 155)
                        .addComponent(finalizar)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(opcaoMaisRapida, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(opcaoRapida))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(OpcaoMenorCusto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(opcaoCusto))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(opcaoMenorCustoBeneficio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(opcaoCustoBeneficio))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(custo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(lucro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(finalizar)
                .addGap(31, 31, 31))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void opcaoRapidaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opcaoRapidaActionPerformed
        EfetuarCalculo(opcaoMaisRapida.getText());
        finalizar.setEnabled(true);
    }//GEN-LAST:event_opcaoRapidaActionPerformed

    private void opcaoCustoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opcaoCustoActionPerformed
        EfetuarCalculo(opcaoMaisRapida.getText());
        finalizar.setEnabled(true);
    }//GEN-LAST:event_opcaoCustoActionPerformed

    private void opcaoCustoBeneficioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opcaoCustoBeneficioActionPerformed
        EfetuarCalculo(opcaoMenorCustoBeneficio.getText());
        finalizar.setEnabled(true);
    }//GEN-LAST:event_opcaoCustoBeneficioActionPerformed

    public String getEscolha() {
        return Escolha;
    }

    public void setEscolha(String Escolha) {
        this.Escolha = Escolha;
    }

    
    
    private void finalizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_finalizarActionPerformed
        if(opcaoRapida.isSelected()){
            setEscolha(opcaoMaisRapida.getText());
        }
        else if(opcaoCusto.isSelected()){
            setEscolha(OpcaoMenorCusto.getText());
        }
        else if(opcaoCusto.isSelected()){
            setEscolha(opcaoMenorCustoBeneficio.getText());
        }   
            
        setVisible(false);
    }//GEN-LAST:event_finalizarActionPerformed
    
    private void custoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_custoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_custoActionPerformed

    public void setRapida(String i){
        opcaoMaisRapida.setText(i);
    }
    private void opcaoMaisRapidaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opcaoMaisRapidaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_opcaoMaisRapidaActionPerformed

    public void setMCusto(String i){
        OpcaoMenorCusto.setText(i);
    }
    
    private void OpcaoMenorCustoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OpcaoMenorCustoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_OpcaoMenorCustoActionPerformed

    public void setCBeneficio(String i){
        opcaoMenorCustoBeneficio.setText(i);
    }
    
    private void opcaoMenorCustoBeneficioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opcaoMenorCustoBeneficioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_opcaoMenorCustoBeneficioActionPerformed
    
    private void lucroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lucroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_lucroActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Calcular.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Calcular.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Calcular.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Calcular.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Calcular().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField OpcaoMenorCusto;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JTextField custo;
    private javax.swing.JButton finalizar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField lucro;
    private javax.swing.JRadioButton opcaoCusto;
    private javax.swing.JRadioButton opcaoCustoBeneficio;
    private javax.swing.JTextField opcaoMaisRapida;
    private javax.swing.JTextField opcaoMenorCustoBeneficio;
    private javax.swing.JRadioButton opcaoRapida;
    // End of variables declaration//GEN-END:variables
}
