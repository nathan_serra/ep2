package transportadora;

/**
 *
 * @author nathan
 */
public abstract class Veiculo {
    private String Nome;
    private String Combustivel;
    private float Rendimento;
    private float VelocidadeMedia;
    private float CargaMaxima;
    
    public Veiculo(){
        
    }
    
    public Veiculo(String Nome, String Combustivel, float Rendimento, float VelocidadeMedia, float CargaMaxima){
        setNome(Nome);
        setCombustivel(Combustivel);
        setRendimento(Rendimento);
        setVelocidadeMedia(VelocidadeMedia);
        setCargaMaxima(CargaMaxima);
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String Nome) {
        this.Nome = Nome;
    }
    
    public float calcularCusto(float DistanciaViagem){
        float Rendimento;
        Rendimento = getRendimento();
        float PrecoCombustivel = 3.0f;
        float QtdCombustivel;
        
        if(getCombustivel() == "gasolina")
            PrecoCombustivel = 4.449f;
        
        else if(getCombustivel() == "alcool")
            PrecoCombustivel = 3.499f;
        
        else if(getCombustivel() == "disel")
            PrecoCombustivel = 3.869f;
        
       
        
        QtdCombustivel = DistanciaViagem/Rendimento;
        
       return QtdCombustivel * PrecoCombustivel; 
    }
    
    public float calcularTempo(float DistanciaViagem){
        float v;
        v = getVelocidadeMedia();
        return DistanciaViagem/v;        
    }

    public String getCombustivel() {
        return Combustivel;
    }

    public void setCombustivel(String Combustivel) {
        this.Combustivel = Combustivel;
    }

    public float getRendimento() {
        return Rendimento;
    }

    public void setRendimento(float Rendimento) {
        this.Rendimento = Rendimento;
    }

    public float getVelocidadeMedia() {
        return VelocidadeMedia;
    }

    public void setVelocidadeMedia(float VelocidadeMedia) {
        this.VelocidadeMedia = VelocidadeMedia;
    }

    public float getCargaMaxima() {
        return CargaMaxima;
    }

    public void setCargaMaxima(float CargaMaxima) {
        this.CargaMaxima = CargaMaxima;
    }
    
}
