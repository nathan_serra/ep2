/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transportadora;

/**
 *
 * @author nathan
 */
public class Van extends Veiculo {
    private float RendimentoPerdido;
    
    
    public Van(){
        setNome("van");
        setCombustivel("diesel");
        setRendimento(10.0f);
        setVelocidadeMedia(80.0f);
        setCargaMaxima(3500.0f);
        setRendimentoPerdido(0.001f);
    }
    
    public float calcularCusto(float DistanciaViagem, float QtdCarga){
        float Rendimento = getRendimento();
        float PrecoCombustivel;
        float QtdCombustivel;
        float RendimentoPerdido = getRendimentoPerdido();
        PrecoCombustivel = 3.869f;
        Rendimento = Rendimento - RendimentoPerdido * QtdCarga;
        QtdCombustivel = DistanciaViagem/Rendimento;
       return QtdCombustivel * PrecoCombustivel;
    }

    public float getRendimentoPerdido() {
        return RendimentoPerdido;
    }

    public void setRendimentoPerdido(float RendimentoPerdido) {
        this.RendimentoPerdido = RendimentoPerdido;
    }
    
    
}
