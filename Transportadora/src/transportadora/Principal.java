package transportadora;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;


public class Principal extends javax.swing.JFrame {
    
    ArrayList<Veiculo> ListaFrota;
    ArrayList<Veiculo> ListaFrete;

    public Principal() {
        initComponents();
        setLocationRelativeTo(null);
        ListaFrota = new ArrayList();
        ListaFrete = new ArrayList();
        ManipulaInterfaceFrota("navegar");
        ManipulaInterfaceFrete("navegar");
        LoadTableFrotaArquivo();
    }
    
    public void LoadTableFrotaArquivo(){
         File file = new File("Frota.txt");
        
        try{
            
            FileReader fr = new FileReader(file);
            BufferedReader bf = new BufferedReader(fr);
            
            String linha = bf.readLine();
            int i = 0;
            int aux = 0;
            
            while(linha != null){
                
                switch (i) {
                    case 0:
                        aux = Integer.parseInt(linha);
                        for(int j = 0; j < aux; j++){
                            ListaFrota.add(new Carro());
                        }
                        break;
                    case 1:
                        aux = Integer.parseInt(linha);
                        for(int j = 0; j < aux; j++){
                            ListaFrota.add(new CarroAlcool());
                        }
                        break;
                    case 2:
                        aux = Integer.parseInt(linha);
                        for(int j = 0; j < aux; j++){
                            ListaFrota.add(new MotoGasolina());
                        }
                        break;
                    case 3:
                        aux = Integer.parseInt(linha);
                        for(int j = 0; j < aux; j++){
                            ListaFrota.add(new MotoAlcool());
                        }
                        break;
                    case 4:
                        aux = Integer.parseInt(linha);
                        for(int j = 0; j < aux; j++){
                            ListaFrota.add(new Van());
                        }
                        break;
                    case 5:
                        aux = Integer.parseInt(linha);
                        for(int j = 0; j < aux; j++){
                            ListaFrota.add(new Carreta());
                        }
                        break;
                    default:
                        break;
                }
                
                linha = bf.readLine();
                i++;
            }
            bf.close();
            fr.close();
            
            LoadTableFrota();  
        }
        catch(IOException ex){
            
        }
    }
    
    public void GuardarFrotaArquivo(){
        File file = new File("Frota.txt");
        ArrayList< Integer > veiculos;
        veiculos = new ArrayList();
        
        try{
            FileWriter fw = new FileWriter(file, true);
            fw.close();
            
            FileWriter fw1 = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fw1);
            
            
            int carrog = 0;
            int carroa = 0;
            int motog = 0;
            int motoa = 0;
            int van = 0;
            int carreta = 0;
            
            for(int j = 0; j < ListaFrota.size(); j++){
                if(ListaFrota.get(j).getNome() == "carro" && ListaFrota.get(j).getCombustivel() == "gasolina")
                    carrog++;
                else if(ListaFrota.get(j).getNome() == "carro" && ListaFrota.get(j).getCombustivel() == "alcool")
                    carroa++;
                else if(ListaFrota.get(j).getNome() == "moto" && ListaFrota.get(j).getCombustivel() == "gasolina")
                    motog++;
                else if(ListaFrota.get(j).getNome() == "moto" && ListaFrota.get(j).getCombustivel() == "alcool")
                    motoa++;
                else if(ListaFrota.get(j).getNome() == "van" && ListaFrota.get(j).getCombustivel() == "diesel")
                    van++;
                else if(ListaFrota.get(j).getNome() == "carreta" && ListaFrota.get(j).getCombustivel() == "diesel")
                    carreta++;
            }
            
            veiculos.add(carrog);
            veiculos.add(carroa);
            veiculos.add(motog);
            veiculos.add(motoa);
            veiculos.add(van);
            veiculos.add(carreta);
            
            for(int i = 0; i < 6; i++){
            bw.write(Integer.toString(veiculos.get(i)));    
            bw.newLine();
                
            }
            bw.close();
            fw1.close();
            
            
        }
        catch(IOException ex){
            
        }
    }
    
    public void LoadTableFrota(){
        Object colunas[] = {"nome", "combustivel"};
        DefaultTableModel modelo = new DefaultTableModel(colunas, 0);
        
        for(int i=0; i<ListaFrota.size() ;i++){
            Object linha[] = new Object[]{ListaFrota.get(i).getNome(), ListaFrota.get(i).getCombustivel()};
            
            modelo.addRow(linha);    
        }
        
        frota.setModel(modelo);
        GuardarFrotaArquivo();
        
    }
    
    public void LoadTabelaFrete(){
        Object colunas[] = {"nome", "combustivel"};
        DefaultTableModel modelo = new DefaultTableModel(colunas, 0);
        
        for(int i=0; i<ListaFrete.size() ;i++){
            Object linha[] = new Object[]{ListaFrete.get(i).getNome(), ListaFrete.get(i).getCombustivel()};
            
            modelo.addRow(linha);    
        }
        
        frete.setModel(modelo);
    }

    public void ManipulaInterfaceFrota(String modo){
        switch(modo){
            case "navegar":
                
                carroRadioButton.setEnabled(false);
                motoRadioButton.setEnabled(false);
                vanRadioButton.setEnabled(false);
                carretaRadioButton.setEnabled(false);
        
                gasolinaRadioButton.setEnabled(false);
                alcoolRadioButton.setEnabled(false);
                dieselRadioButton.setEnabled(false);
        
                botaoSalvar.setEnabled(false);
                botaoCancelarFrota.setEnabled(false);
        
                botaoExcluirFrota.setEnabled(false);
                
                carroRadioButton.setSelected(false);
                motoRadioButton.setSelected(false);
                vanRadioButton.setSelected(false);
                carretaRadioButton.setSelected(false);
        
                gasolinaRadioButton.setSelected(false);
                alcoolRadioButton.setSelected(false);
                dieselRadioButton.setSelected(false);
                
                break;
            
            case"novo":
                carroRadioButton.setEnabled(true);
                motoRadioButton.setEnabled(true);
                vanRadioButton.setEnabled(true);
                carretaRadioButton.setEnabled(true);
        
                gasolinaRadioButton.setEnabled(true);
                alcoolRadioButton.setEnabled(true);
                dieselRadioButton.setEnabled(true);
                
                botaoExcluirFrota.setEnabled(false);
        
                carroRadioButton.setSelected(false);
                motoRadioButton.setSelected(false);
                vanRadioButton.setSelected(false);
                carretaRadioButton.setSelected(false);
        
                gasolinaRadioButton.setSelected(false);
                alcoolRadioButton.setSelected(false);
                dieselRadioButton.setSelected(false);
        
                botaoSalvar.setEnabled(true);
                botaoCancelarFrota.setEnabled(true);
            
                break;
                
            case "cancelar":
                carroRadioButton.setEnabled(false);
                motoRadioButton.setEnabled(false);
                vanRadioButton.setEnabled(false);
                carretaRadioButton.setEnabled(false);
        
                gasolinaRadioButton.setEnabled(false);
                alcoolRadioButton.setEnabled(false);
                dieselRadioButton.setEnabled(false);
        
                botaoSalvar.setEnabled(false);
                botaoCancelarFrota.setEnabled(false);
                
                botaoExcluirFrota.setEnabled(false);
                
                carroRadioButton.setSelected(false);
                motoRadioButton.setSelected(false);
                vanRadioButton.setSelected(false);
                carretaRadioButton.setSelected(false);
        
                gasolinaRadioButton.setSelected(false);
                alcoolRadioButton.setSelected(false);
                dieselRadioButton.setSelected(false);
                
                break;
                
            case "salvar":
                carroRadioButton.setEnabled(false);
                motoRadioButton.setEnabled(false);
                vanRadioButton.setEnabled(false);
                carretaRadioButton.setEnabled(false);
        
                gasolinaRadioButton.setEnabled(false);
                alcoolRadioButton.setEnabled(false);
                dieselRadioButton.setEnabled(false);
        
                botaoSalvar.setEnabled(false);
                botaoCancelarFrota.setEnabled(false);
                
                botaoExcluirFrota.setEnabled(false);
                
                carroRadioButton.setSelected(false);
                motoRadioButton.setSelected(false);
                vanRadioButton.setSelected(false);
                carretaRadioButton.setSelected(false);
        
                gasolinaRadioButton.setSelected(false);
                alcoolRadioButton.setSelected(false);
                dieselRadioButton.setSelected(false);
                
                break;
            
            case "excluir":
                carroRadioButton.setEnabled(false);
                motoRadioButton.setEnabled(false);
                vanRadioButton.setEnabled(false);
                carretaRadioButton.setEnabled(false);
        
                gasolinaRadioButton.setEnabled(false);
                alcoolRadioButton.setEnabled(false);
                dieselRadioButton.setEnabled(false);
        
                botaoSalvar.setEnabled(false);
                botaoCancelarFrota.setEnabled(false);
                
                botaoExcluirFrota.setEnabled(true);
                
                carroRadioButton.setSelected(false);
                motoRadioButton.setSelected(false);
                vanRadioButton.setSelected(false);
                carretaRadioButton.setSelected(false);
        
                gasolinaRadioButton.setSelected(false);
                alcoolRadioButton.setSelected(false);
                dieselRadioButton.setSelected(false);
                
                break;
                
            default: System.out.println("modo invalido");
                
        }
        
    }
    
    public void ManipulaInterfaceFrete(String modo){
        switch(modo){
            case "navegar":
                peso.setEnabled(false);
                tempoMaximo.setEnabled(false);
                distancia.setEnabled(false);
                lucro.setEnabled(false);
                
                botaoCancelarFrete.setEnabled(false);
                botaoCalcular.setEnabled(false);
                botaoLimpar.setEnabled(false);
                
                break;
            
            case"novo":
                peso.setEnabled(true);
                tempoMaximo.setEnabled(true);
                distancia.setEnabled(true);
                lucro.setEnabled(true);
                
                botaoCancelarFrete.setEnabled(true);
                botaoCalcular.setEnabled(true);
                botaoLimpar.setEnabled(true);
            
                break;
            
            case "excluir":
                peso.setEnabled(false);
                tempoMaximo.setEnabled(false);
                distancia.setEnabled(false);
                lucro.setEnabled(false);
                
                botaoCancelarFrete.setEnabled(false);
                botaoCalcular.setEnabled(false);
                botaoLimpar.setEnabled(false);
                
                
                break;
                
            default: System.out.println("modo invalido");
        }    
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList<>();
        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        jPanel = new javax.swing.JPanel();
        botaoNovoFrota = new javax.swing.JButton();
        botaoExcluirFrota = new javax.swing.JButton();
        jDesktopPane1 = new javax.swing.JDesktopPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        frota = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        botaoCancelarFrota = new javax.swing.JButton();
        botaoSalvar = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        gasolinaRadioButton = new javax.swing.JRadioButton();
        alcoolRadioButton = new javax.swing.JRadioButton();
        dieselRadioButton = new javax.swing.JRadioButton();
        carroRadioButton = new javax.swing.JRadioButton();
        motoRadioButton = new javax.swing.JRadioButton();
        vanRadioButton = new javax.swing.JRadioButton();
        carretaRadioButton = new javax.swing.JRadioButton();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        frete = new javax.swing.JTable();
        botaoNovoFrete = new javax.swing.JToggleButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        tempoMaximo = new javax.swing.JTextField();
        distancia = new javax.swing.JTextField();
        lucro = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        botaoCalcular = new javax.swing.JButton();
        botaoLimpar = new javax.swing.JButton();
        peso = new javax.swing.JTextField();
        botaoCancelarFrete = new javax.swing.JButton();

        jList1.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane2.setViewportView(jList1);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("TRANSPORTADORA");

        jPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Frota disponivel"));

        botaoNovoFrota.setText("novo");
        botaoNovoFrota.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoNovoFrotaActionPerformed(evt);
            }
        });

        botaoExcluirFrota.setText("excluir");
        botaoExcluirFrota.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoExcluirFrotaActionPerformed(evt);
            }
        });

        frota.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nome", "Combustível"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        frota.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                frotaMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(frota);
        if (frota.getColumnModel().getColumnCount() > 0) {
            frota.getColumnModel().getColumn(0).setResizable(false);
            frota.getColumnModel().getColumn(1).setResizable(false);
        }

        javax.swing.GroupLayout jPanelLayout = new javax.swing.GroupLayout(jPanel);
        jPanel.setLayout(jPanelLayout);
        jPanelLayout.setHorizontalGroup(
            jPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelLayout.createSequentialGroup()
                .addComponent(jDesktopPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(botaoNovoFrota, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(botaoExcluirFrota, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(jPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 305, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelLayout.setVerticalGroup(
            jPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jDesktopPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(botaoExcluirFrota)
                        .addComponent(botaoNovoFrota)))
                .addGap(8, 8, 8))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Novo veículo"));

        jLabel1.setText("carro");

        jLabel2.setText("moto:");

        botaoCancelarFrota.setText("cancelar");
        botaoCancelarFrota.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoCancelarFrotaActionPerformed(evt);
            }
        });

        botaoSalvar.setText("salvar");
        botaoSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoSalvarActionPerformed(evt);
            }
        });

        jLabel7.setText("van:");

        jLabel8.setText("carreta:");

        buttonGroup1.add(gasolinaRadioButton);
        gasolinaRadioButton.setText("gasolina");

        buttonGroup1.add(alcoolRadioButton);
        alcoolRadioButton.setText("alcool");

        buttonGroup1.add(dieselRadioButton);
        dieselRadioButton.setText("diesel");

        buttonGroup2.add(carroRadioButton);
        carroRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                carroRadioButtonActionPerformed(evt);
            }
        });

        buttonGroup2.add(motoRadioButton);
        motoRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                motoRadioButtonActionPerformed(evt);
            }
        });

        buttonGroup2.add(vanRadioButton);
        vanRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                vanRadioButtonActionPerformed(evt);
            }
        });

        buttonGroup2.add(carretaRadioButton);
        carretaRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                carretaRadioButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(botaoSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(botaoCancelarFrota, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(carroRadioButton))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(motoRadioButton, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(vanRadioButton, javax.swing.GroupLayout.Alignment.TRAILING)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(gasolinaRadioButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(alcoolRadioButton))
                            .addComponent(jLabel8))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(dieselRadioButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(carretaRadioButton)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(carroRadioButton)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(motoRadioButton)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(vanRadioButton)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(gasolinaRadioButton)
                            .addComponent(alcoolRadioButton)
                            .addComponent(dieselRadioButton))
                        .addContainerGap())
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(carretaRadioButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(botaoSalvar)
                            .addComponent(botaoCancelarFrota)))))
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Fretes em andamento"));

        frete.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nome", "Combustivel"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        frete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                freteMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(frete);
        if (frete.getColumnModel().getColumnCount() > 0) {
            frete.getColumnModel().getColumn(0).setResizable(false);
            frete.getColumnModel().getColumn(1).setResizable(false);
        }

        botaoNovoFrete.setText("novo");
        botaoNovoFrete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoNovoFreteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(botaoNovoFrete, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(botaoNovoFrete)
                .addContainerGap())
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Novo frete"));

        jLabel3.setText("Peso (Kg):");

        tempoMaximo.setText(" ");
        tempoMaximo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tempoMaximoActionPerformed(evt);
            }
        });

        distancia.setText(" ");
        distancia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                distanciaActionPerformed(evt);
            }
        });

        lucro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lucroActionPerformed(evt);
            }
        });

        jLabel4.setText("Tempo maximo (horas):");

        jLabel6.setText("Distancia (Km):");

        jLabel5.setText("Lucro (R$):");

        botaoCalcular.setText("calcular");
        botaoCalcular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoCalcularActionPerformed(evt);
            }
        });

        botaoLimpar.setText("limpar");
        botaoLimpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoLimparActionPerformed(evt);
            }
        });

        peso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pesoActionPerformed(evt);
            }
        });

        botaoCancelarFrete.setText("cancelar");
        botaoCancelarFrete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoCancelarFreteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(58, 58, 58)
                        .addComponent(tempoMaximo, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel3)
                            .addGap(151, 151, 151)
                            .addComponent(peso, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel6)
                            .addGap(118, 118, 118)
                            .addComponent(distancia, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel5)
                            .addGap(147, 147, 147)
                            .addComponent(lucro, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(botaoCalcular, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(botaoLimpar, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(botaoCancelarFrete, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(7, 7, 7))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(peso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tempoMaximo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(distancia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(lucro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botaoCalcular)
                    .addComponent(botaoLimpar)
                    .addComponent(botaoCancelarFrete)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(12, 12, 12))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botaoExcluirFrotaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoExcluirFrotaActionPerformed
        int aux = frota.getSelectedRow();
        
        if(aux >= 0 && aux <= ListaFrota.size()){
            ListaFrota.remove(aux);
        } 
        ManipulaInterfaceFrota("navegar");
        LoadTableFrota();
    }//GEN-LAST:event_botaoExcluirFrotaActionPerformed

    private void botaoNovoFrotaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoNovoFrotaActionPerformed
        ManipulaInterfaceFrota("novo");
         
    }//GEN-LAST:event_botaoNovoFrotaActionPerformed

    private void distanciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_distanciaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_distanciaActionPerformed

    private void botaoLimparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoLimparActionPerformed
        peso.setText("");
        distancia.setText("");
        lucro.setText("");
        tempoMaximo.setText("");
    }//GEN-LAST:event_botaoLimparActionPerformed

    public String MelhorCusto(boolean moto, boolean motoAlcool, boolean carro, boolean carroAlcool, boolean van, boolean carreta, float distancia, float carga){
        Carro c = new Carro();
        CarroAlcool ca = new CarroAlcool();
        MotoGasolina mg = new MotoGasolina();
        MotoAlcool ma = new MotoAlcool();
        Van v = new Van();
        Carreta cr = new Carreta();
        
        float aux = 0.0f;
        String veiculo = null;
        
        if(carro){
           if(aux != 0 && aux > c.calcularCusto(distancia, carga)){
               aux = c.calcularCusto(distancia, carga);
               veiculo = "carro gasolina";
            }
           else if(aux == 0){
                aux = c.calcularCusto(distancia, carga);
                veiculo = "carro gasolina";
            }
        }       
        if(carroAlcool){
            if(aux != 0 && aux > ca.calcularCusto(distancia, carga)){
               aux = ca.calcularCusto(distancia, carga);
               veiculo = "carro alcool";
            }
           else if(aux == 0){
                aux = ca.calcularCusto(distancia, carga);
                veiculo = "carro alcool";
            }
        }    
        if(moto){
            if(aux != 0 && aux > mg.calcularCusto(distancia, carga)){
               aux = mg.calcularCusto(distancia, carga);
               veiculo = "moto gasolina";
            }
           else if(aux == 0){
                aux = mg.calcularCusto(distancia, carga);
                veiculo = "moto gasolina";
            }
        }    
        if(motoAlcool){
            if(aux != 0 && aux > ma.calcularCusto(distancia, carga)){
               aux = ma.calcularCusto(distancia, carga);
               veiculo = "moto alcool";
            }
           else if(aux == 0){
                aux = ma.calcularCusto(distancia, carga);
                veiculo = "moto alcool";
            }
        }    
        if(van){
            if(aux != 0 && aux > v.calcularCusto(distancia, carga)){
               aux = v.calcularCusto(distancia, carga);
               veiculo = v.getNome();
            }
           else if(aux == 0){
                aux = v.calcularCusto(distancia, carga);
                veiculo = v.getNome();
            }
        }    
        if(carreta){
            if(aux != 0 && aux > cr.calcularCusto(distancia, carga)){
               aux = cr.calcularCusto(distancia, carga);
               veiculo = cr.getNome();
            }
           else if(aux == 0){
                aux = cr.calcularCusto(distancia, carga);
                veiculo = cr.getNome();
            }
        }
        
        return veiculo;
    }
   
    public String MaisRapido(boolean moto, boolean motoAlcool, boolean carro, boolean carroAlcool, boolean van, boolean carreta, float diastancia){
        Carro c = new Carro();
        CarroAlcool ca = new CarroAlcool();
        MotoGasolina mg = new MotoGasolina();
        MotoAlcool ma = new MotoAlcool();
        Van v = new Van();
        Carreta cr = new Carreta();
        
        float aux = 0.0f;
        String veiculo = null;
        
        if(carro){
           if(aux != 0 && aux > c.calcularTempo(diastancia)){
               aux = c.calcularTempo(diastancia);
               veiculo = "carro gasolina";
            }
           else if(aux == 0){
                aux = c.calcularTempo(diastancia);
                veiculo = "carro gasolina";
            }
        }
        if(carroAlcool){
           if(aux != 0 && aux > ca.calcularTempo(diastancia)){
               aux = ca.calcularTempo(diastancia);
               veiculo = "carro alcool";
            }
           else if(aux == 0){
                aux = ca.calcularTempo(diastancia);
                veiculo = "carro alcool";
            }
        }
        if(moto){
           if(aux != 0 && aux > mg.calcularTempo(diastancia)){
               aux = mg.calcularTempo(diastancia);
               veiculo = "moto gasolina";
            }
           else if(aux == 0){
                aux = mg.calcularTempo(diastancia);
                veiculo = "moto gasolina";
            }
        }
        if(motoAlcool){
           if(aux != 0 && aux > ma.calcularTempo(diastancia)){
               aux = ma.calcularTempo(diastancia);
               veiculo = "moto alcool";
            }
           else if(aux == 0){
                aux = ma.calcularTempo(diastancia);
                veiculo = "moto alcool";
            }
        }
        if(van){
           if(aux != 0 && aux > v.calcularTempo(diastancia)){
               aux = v.calcularTempo(diastancia);
               veiculo = v.getNome();
            }
           else if(aux == 0){
                aux = v.calcularTempo(diastancia);
                veiculo = v.getNome();
            }
        }
        if(carreta){
           if(aux != 0 && aux > cr.calcularTempo(diastancia)){
               aux = cr.calcularTempo(diastancia);
               veiculo = cr.getNome();
            }
           else if(aux == 0){
                aux = cr.calcularTempo(diastancia);
                veiculo = cr.getNome();
            }
        }
        
        return veiculo;
    }
    
    public String MelhorCustoBeneficio(boolean moto, boolean motoAlcool, boolean carro, boolean carroAlcool, boolean van, boolean carreta, float distancia, float carga){
        Carro c = new Carro();
        CarroAlcool ca = new CarroAlcool();
        MotoGasolina mg = new MotoGasolina();
        MotoAlcool ma = new MotoAlcool();
        Van v = new Van();
        Carreta cr = new Carreta();
        
        float aux = 0.0f;
        float aux1 = 0.0f;
        String veiculo1 = null;
        String veiculo = null;
        
        
        if(carro){
           if(aux1 != 0 && aux1 > c.calcularTempo(distancia)){
               aux1 = c.calcularTempo(distancia);
               veiculo1 = "carro gasolina";
            }
           else if(aux1 == 0){
                aux1 = c.calcularTempo(distancia);
                veiculo1 = "carro gasolina";
            }
        }
        if(carroAlcool){
           if(aux1 != 0 && aux1 > ca.calcularTempo(distancia)){
               aux1 = ca.calcularTempo(distancia);
               veiculo1 = "carro alcool";
            }
           else if(aux1 == 0){
                aux1 = ca.calcularTempo(distancia);
                veiculo1 = "carro alcool";
            }
        }
        if(moto){
           if(aux1 != 0 && aux1 > mg.calcularTempo(distancia)){
               aux1 = mg.calcularTempo(distancia);
               veiculo1 = "moto gasolina";
            }
           else if(aux1 == 0){
                aux1 = mg.calcularTempo(distancia);
                veiculo1 = "moto gasolina";
            }
        }
        if(motoAlcool){
           if(aux1 != 0 && aux1 > ma.calcularTempo(distancia)){
               aux1 = ma.calcularTempo(distancia);
               veiculo1 = "moto alcool";
            }
           else if(aux1 == 0){
                aux1 = ma.calcularTempo(distancia);
                veiculo1 = "moto alcool";
            }
        }
        if(van){
           if(aux1 != 0 && aux1 > v.calcularTempo(distancia)){
               aux1 = v.calcularTempo(distancia);
               veiculo1 = v.getNome();
            }
           else if(aux1 == 0){
                aux1 = v.calcularTempo(distancia);
                veiculo1 = v.getNome();
            }
        }
        if(carreta){
           if(aux1 != 0 && aux1 > cr.calcularTempo(distancia)){
               aux1 = cr.calcularTempo(distancia);
               veiculo1 = cr.getNome();
            }
           else if(aux == 0){
                aux1 = cr.calcularTempo(distancia);
                veiculo1 = cr.getNome();
            }
        }
        
        
        if(carro){
           if(aux != 0 && aux > c.calcularCusto(distancia, carga)){
               aux = c.calcularCusto(distancia, carga);
               veiculo = "carro gasolina";
            }
           else if(aux == 0){
                aux = c.calcularCusto(distancia, carga);
                veiculo = "carro gasolina";
            }
        }       
        if(carroAlcool){
            if(aux != 0 && aux > ca.calcularCusto(distancia, carga)){
               aux = ca.calcularCusto(distancia, carga);
               veiculo = "carro alcool";
            }
           else if(aux == 0){
                aux = ca.calcularCusto(distancia, carga);
                veiculo = "carro alcool";
            }
        }    
        if(moto){
            if(aux != 0 && aux > mg.calcularCusto(distancia, carga)){
               aux = mg.calcularCusto(distancia, carga);
               veiculo = "moto gasolina";
            }
           else if(aux == 0){
                aux = mg.calcularCusto(distancia, carga);
                veiculo = "moto gasolina";
            }
        }    
        if(motoAlcool){
            if(aux != 0 && aux > ma.calcularCusto(distancia, carga)){
               aux = ma.calcularCusto(distancia, carga);
               veiculo = "moto alcool";
            }
           else if(aux == 0){
                aux = ma.calcularCusto(distancia, carga);
                veiculo = "moto alcool";
            }
        }    
        if(van){
            if(aux != 0 && aux > v.calcularCusto(distancia, carga)){
               aux = v.calcularCusto(distancia, carga);
               veiculo = v.getNome();
            }
           else if(aux == 0){
                aux = v.calcularCusto(distancia, carga);
                veiculo = v.getNome();
            }
        }    
        if(carreta){
            if(aux != 0 && aux > cr.calcularCusto(distancia, carga)){
               aux = cr.calcularCusto(distancia, carga);
               veiculo = cr.getNome();
            }
           else if(aux == 0){
                aux = cr.calcularCusto(distancia, carga);
                veiculo = cr.getNome();
            }
        }
        
        if(veiculo1 == veiculo){
            return veiculo1;
        }
        else{
            return veiculo;
        }
        
    }
    
    public void Escolher(String s){
        boolean ja = true;
        for(int i = 0; i < ListaFrota.size(); i++){
                if(ja){
                    switch(s){
                         case "carro gasolina":
                            if(ListaFrota.get(i).getNome() == "carro" && ListaFrota.get(i).getCombustivel() == "gasolina"){
                                ListaFrota.remove(i);
                                ListaFrete.add(new Carro());
                                ja = false;
                            }
                            break;
                    
                        case "carro alcool":
                           if(ListaFrota.get(i).getNome() == "carro" && ListaFrota.get(i).getCombustivel() == "alcool"){
                                ListaFrota.remove(i);
                                ListaFrete.add(new CarroAlcool());
                                ja = false;
                            }
                            break;
                
                        case "moto gasolina":
                            if(ListaFrota.get(i).getNome() == "moto" && ListaFrota.get(i).getCombustivel() == "gasolina"){
                                ListaFrota.remove(i);
                                ListaFrete.add(new MotoGasolina());
                                ja = false;
                            }
                
                             break;
                        case "moto alcool":
                            if(ListaFrota.get(i).getNome() == "moto" && ListaFrota.get(i).getCombustivel() == "alcool"){
                                ListaFrota.remove(i);
                                ListaFrete.add(new MotoAlcool());
                                ja = false;
                            }
                            break;
                
                        case "van":
                            if(ListaFrota.get(i).getNome() == "van"){
                                ListaFrota.remove(i);
                                ListaFrete.add(new Van());
                                ja = false;
                            }
                
                            break;
                        case "carreta":
                            if(ListaFrota.get(i).getNome() == "carreta"){
                                ListaFrota.remove(i);
                                ListaFrete.add(new Carreta());
                                ja = false;
                            }
                            break;
                        default: System.out.println("erro");
                    }
                }
            }
    }
    
    private void botaoCalcularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoCalcularActionPerformed
        float pesoFloat;
        float distanciaFloat;
        float lucroFloat;
        float tempoMaximoFloat;
        
        try{
            pesoFloat = Float.parseFloat(peso.getText());
            distanciaFloat = Float.parseFloat(distancia.getText());
            lucroFloat = Float.parseFloat(lucro.getText());
            tempoMaximoFloat = Float.parseFloat(tempoMaximo.getText());
            
            float vel = distanciaFloat/tempoMaximoFloat;
           
            boolean moto = false;
            boolean motoAlcool = false;
            boolean carro = false;
            boolean carroAlcool = false;
            boolean van = false;
            boolean carreta = false;
           
            String Veiculo;
            String Combustivel;
           
            for(int i = 0; i < ListaFrota.size(); i ++){
               Veiculo = ListaFrota.get(i).getNome();
               Combustivel = ListaFrota.get(i).getCombustivel();
               
                if(Veiculo == "moto" && Combustivel == "gasolina"){
                   
                    if(pesoFloat > 50 || vel > 110){
                        moto = false;
                    }
                    else{
                        moto = true;
                    }
                    
                }
                else if(Veiculo == "moto" && Combustivel == "alcool"){
                   
                    if(pesoFloat > 50 || vel > 110){
                        motoAlcool = false;
                    }
                    else{
                        motoAlcool = true;
                    }
                    
                }
                else if(Veiculo == "carro" && Combustivel == "gasolina"){
                   
                    if(pesoFloat > 360 || vel > 100){
                        carro = false;
                    }
                    else{
                        carro = true;
                    }
                    
                }
                else if(Veiculo == "carro" && Combustivel == "alcool"){
                   
                    if(pesoFloat > 360 || vel > 100){
                        carroAlcool = false;
                    }
                    else{
                        carroAlcool = true;
                    }
                   
                }
                else if(Veiculo == "van"){
                  
                    if(pesoFloat > 3500 || vel > 80){
                        van = false;
                    }
                    else{
                        van = true;
                    }
                    
                }
                else if(Veiculo == "Carreta"){
                   
                    if(pesoFloat > 30000 || vel > 60){
                        carreta = false;
                    }
                    else{
                        carreta = true;
                    }
                    
                }
               
            }
            
            Calcular c = new Calcular();
            
            c.setRapida(MaisRapido( moto, motoAlcool, carro, carroAlcool, van, carreta, distanciaFloat));
            c.setMCusto(MelhorCusto( moto, motoAlcool, carro, carroAlcool, van, carreta, distanciaFloat, pesoFloat));
            c.setCBeneficio(MelhorCusto( moto, motoAlcool, carro, carroAlcool, van, carreta, distanciaFloat, pesoFloat));
            c.setPesoFloat(pesoFloat);
            c.setDistanciaFloat(distanciaFloat);
            c.setLucroFloat(lucroFloat);
            c.setTempoMaximoFloat(tempoMaximoFloat);
            c.setVisible(true);
            
            Escolher(c.getEscolha());
            
            LoadTableFrota();
            LoadTabelaFrete();
            
           
           ManipulaInterfaceFrete("navegar");
           peso.setText(" ");
           distancia.setText(" ");
           lucro.setText(" ");
           tempoMaximo.setText(" ");
        }
        catch(java.lang.NumberFormatException e){
            new ErroCalcular().setVisible(true);
            
            peso.setText(" ");
            distancia.setText(" ");
            lucro.setText(" ");
            tempoMaximo.setText(" ");
        }

        
    }//GEN-LAST:event_botaoCalcularActionPerformed

    private void tempoMaximoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tempoMaximoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tempoMaximoActionPerformed

    private void lucroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lucroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_lucroActionPerformed

    private void pesoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pesoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_pesoActionPerformed

    private void carroRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_carroRadioButtonActionPerformed
        gasolinaRadioButton.setEnabled(true);
        alcoolRadioButton.setEnabled(true);
        dieselRadioButton.setSelected(false);
        dieselRadioButton.setEnabled(false);
    }//GEN-LAST:event_carroRadioButtonActionPerformed

    private void motoRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_motoRadioButtonActionPerformed
        gasolinaRadioButton.setEnabled(true);
        alcoolRadioButton.setEnabled(true);
        dieselRadioButton.setSelected(false);
        dieselRadioButton.setEnabled(false);
    }//GEN-LAST:event_motoRadioButtonActionPerformed

    private void vanRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_vanRadioButtonActionPerformed
        gasolinaRadioButton.setEnabled(false);
        alcoolRadioButton.setEnabled(false);
        dieselRadioButton.setEnabled(true);
        dieselRadioButton.setSelected(true);
    }//GEN-LAST:event_vanRadioButtonActionPerformed

    private void carretaRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_carretaRadioButtonActionPerformed
        gasolinaRadioButton.setEnabled(false);
        alcoolRadioButton.setEnabled(false);
        dieselRadioButton.setEnabled(true);        
        dieselRadioButton.setSelected(true);
    }//GEN-LAST:event_carretaRadioButtonActionPerformed

    private void botaoCancelarFrotaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoCancelarFrotaActionPerformed
        ManipulaInterfaceFrota("cancelar");
    }//GEN-LAST:event_botaoCancelarFrotaActionPerformed

    private void botaoSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoSalvarActionPerformed
        if(carroRadioButton.isSelected()){
            if(gasolinaRadioButton.isSelected()){
                Carro carro = new Carro();
                ListaFrota.add(carro);
            }
            else if(alcoolRadioButton.isSelected()){
                CarroAlcool carrroAlcool = new CarroAlcool();
                ListaFrota.add(carrroAlcool);
            }
        }
        
        else if(motoRadioButton.isSelected()){
            if(gasolinaRadioButton.isSelected()){
                MotoGasolina moto = new MotoGasolina();
                ListaFrota.add(moto);
            }
            else if(alcoolRadioButton.isSelected()){
                MotoAlcool moto = new MotoAlcool();
                ListaFrota.add(moto);
            }
        }
        
        else if(vanRadioButton.isSelected()){
            Van van = new Van();
            ListaFrota.add(van);
        }
        
        else if(carretaRadioButton.isSelected()){
            Carreta carreta = new Carreta();
            ListaFrota.add(carreta);
        }
        
        
        LoadTableFrota();
        
        ManipulaInterfaceFrota("salvar");
        
    }//GEN-LAST:event_botaoSalvarActionPerformed

    private void frotaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_frotaMouseClicked
        int aux = frota.getSelectedRow();
        
        if(aux >= 0 && aux <= ListaFrota.size()){
            ManipulaInterfaceFrota("excluir");
        }
    }//GEN-LAST:event_frotaMouseClicked

    private void botaoNovoFreteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoNovoFreteActionPerformed
        ManipulaInterfaceFrete("novo");
    }//GEN-LAST:event_botaoNovoFreteActionPerformed

    private void freteMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_freteMouseClicked

    }//GEN-LAST:event_freteMouseClicked

    private void botaoCancelarFreteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoCancelarFreteActionPerformed
        ManipulaInterfaceFrete("navegar");
        peso.setText("");
        distancia.setText("");
        lucro.setText("");
        tempoMaximo.setText("");
    }//GEN-LAST:event_botaoCancelarFreteActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Principal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton alcoolRadioButton;
    private javax.swing.JButton botaoCalcular;
    private javax.swing.JButton botaoCancelarFrete;
    private javax.swing.JButton botaoCancelarFrota;
    private javax.swing.JButton botaoExcluirFrota;
    private javax.swing.JButton botaoLimpar;
    private javax.swing.JToggleButton botaoNovoFrete;
    private javax.swing.JButton botaoNovoFrota;
    private javax.swing.JButton botaoSalvar;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JRadioButton carretaRadioButton;
    private javax.swing.JRadioButton carroRadioButton;
    private javax.swing.JRadioButton dieselRadioButton;
    private javax.swing.JTextField distancia;
    private javax.swing.JTable frete;
    private javax.swing.JTable frota;
    private javax.swing.JRadioButton gasolinaRadioButton;
    private javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JList<String> jList1;
    private javax.swing.JPanel jPanel;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextField lucro;
    private javax.swing.JRadioButton motoRadioButton;
    private javax.swing.JTextField peso;
    private javax.swing.JTextField tempoMaximo;
    private javax.swing.JRadioButton vanRadioButton;
    // End of variables declaration//GEN-END:variables
}
