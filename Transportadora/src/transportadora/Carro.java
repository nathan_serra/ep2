/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transportadora;

/**
 *
 * @author nathan
 */
public class Carro extends Veiculo {
    private float RendimentoPerdido;
    

    
    public Carro(){
        setNome("carro");
        setCombustivel("gasolina");
        setRendimento(14.0f);
        setVelocidadeMedia(100.0f);
        setCargaMaxima(360.0f);
        setRendimentoPerdido(0.025f);
    }
    
    public float calcularCusto(float DistanciaViagem, float QtdCarga){
        float Rendimento = getRendimento();
        float PrecoCombustivel;
        float QtdCombustivel;
        float RendimentoPerdido = getRendimentoPerdido();
        PrecoCombustivel = 4.449f;
        Rendimento = Rendimento - RendimentoPerdido * QtdCarga;
        QtdCombustivel = DistanciaViagem/Rendimento;
       return QtdCombustivel * PrecoCombustivel;
    }

    public float getRendimentoPerdido() {
        return RendimentoPerdido;
    }

    public void setRendimentoPerdido(float RendimentoPerdido) {
        this.RendimentoPerdido = RendimentoPerdido;
    }
    
}
