package transportadora;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author nathan
 */
public class CarroTest {
    public String Combustivel;
    public float Rendimento;
    public float VelocidadeMedia;
    public float CargaMaxima;
    public float RendimentoPerdido;
    public Carro carro;
    
    @Before
    public void beforeTests(){
        Combustivel = "gasolina";
        Rendimento = 8.0f;
        VelocidadeMedia = 100.0f;
        CargaMaxima = 360.0f;
        RendimentoPerdido = 0.025f;
        carro = new Carro();
    }
    
    @Test
    public void testaConstrutor(){
        
        
        carro = new Carro();
        
        assertEquals(Combustivel, carro.getCombustivel());
        assertEquals(Rendimento, carro.getRendimento(), 10e-5);
        assertEquals(VelocidadeMedia, carro.getVelocidadeMedia(), 10e-5);
        assertEquals(CargaMaxima, carro.getCargaMaxima(), 10e-5);
        assertEquals(RendimentoPerdido, carro.getRendimentoPerdido(), 10e-5);
    }
    
    @Test
    public void testCalculaCusto(){
        carro = new Carro();
        float teste = 44.49f;
        assertEquals(teste, carro.calcularCusto(55, 100), 10e-5);
    }
    
    @After
    public void AfterTests(){
        carro = null;
    }
}
